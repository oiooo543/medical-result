package com.bowen.medicalresults.web.rest;

import com.bowen.medicalresults.MedicalResultsApp;

import com.bowen.medicalresults.domain.Grades;
import com.bowen.medicalresults.repository.GradesRepository;
import com.bowen.medicalresults.service.GradesService;
import com.bowen.medicalresults.service.dto.GradesDTO;
import com.bowen.medicalresults.service.mapper.GradesMapper;
import com.bowen.medicalresults.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.bowen.medicalresults.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GradesResource REST controller.
 *
 * @see GradesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MedicalResultsApp.class)
public class GradesResourceIntTest {

    private static final String DEFAULT_MATNO = "AAAAAAAAAA";
    private static final String UPDATED_MATNO = "BBBBBBBBBB";

    private static final Integer DEFAULT_CA = 1;
    private static final Integer UPDATED_CA = 2;

    private static final Integer DEFAULT_EXAM = 1;
    private static final Integer UPDATED_EXAM = 2;

    private static final Integer DEFAULT_TOTAL = 1;
    private static final Integer UPDATED_TOTAL = 2;

    private static final String DEFAULT_GRADE = "AAAAAAAAAA";
    private static final String UPDATED_GRADE = "BBBBBBBBBB";

    private static final Integer DEFAULT_SEMESTER = 1;
    private static final Integer UPDATED_SEMESTER = 2;

    private static final String DEFAULT_YEAR = "AAAAAAAAAA";
    private static final String UPDATED_YEAR = "BBBBBBBBBB";

    private static final Integer DEFAULT_ATTEMPT = 1;
    private static final Integer UPDATED_ATTEMPT = 2;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private GradesRepository gradesRepository;

    @Autowired
    private GradesMapper gradesMapper;

    @Autowired
    private GradesService gradesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restGradesMockMvc;

    private Grades grades;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final GradesResource gradesResource = new GradesResource(gradesService);
        this.restGradesMockMvc = MockMvcBuilders.standaloneSetup(gradesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Grades createEntity(EntityManager em) {
        Grades grades = new Grades()
            .matno(DEFAULT_MATNO)
            .ca(DEFAULT_CA)
            .exam(DEFAULT_EXAM)
            .total(DEFAULT_TOTAL)
            .grade(DEFAULT_GRADE)
            .semester(DEFAULT_SEMESTER)
            .year(DEFAULT_YEAR)
            .attempt(DEFAULT_ATTEMPT)
            .status(DEFAULT_STATUS);
        return grades;
    }

    @Before
    public void initTest() {
        grades = createEntity(em);
    }

    @Test
    @Transactional
    public void createGrades() throws Exception {
        int databaseSizeBeforeCreate = gradesRepository.findAll().size();

        // Create the Grades
        GradesDTO gradesDTO = gradesMapper.toDto(grades);
        restGradesMockMvc.perform(post("/api/grades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gradesDTO)))
            .andExpect(status().isCreated());

        // Validate the Grades in the database
        List<Grades> gradesList = gradesRepository.findAll();
        assertThat(gradesList).hasSize(databaseSizeBeforeCreate + 1);
        Grades testGrades = gradesList.get(gradesList.size() - 1);
        assertThat(testGrades.getMatno()).isEqualTo(DEFAULT_MATNO);
        assertThat(testGrades.getCa()).isEqualTo(DEFAULT_CA);
        assertThat(testGrades.getExam()).isEqualTo(DEFAULT_EXAM);
        assertThat(testGrades.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testGrades.getGrade()).isEqualTo(DEFAULT_GRADE);
        assertThat(testGrades.getSemester()).isEqualTo(DEFAULT_SEMESTER);
        assertThat(testGrades.getYear()).isEqualTo(DEFAULT_YEAR);
        assertThat(testGrades.getAttempt()).isEqualTo(DEFAULT_ATTEMPT);
        assertThat(testGrades.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createGradesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gradesRepository.findAll().size();

        // Create the Grades with an existing ID
        grades.setId(1L);
        GradesDTO gradesDTO = gradesMapper.toDto(grades);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGradesMockMvc.perform(post("/api/grades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gradesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Grades in the database
        List<Grades> gradesList = gradesRepository.findAll();
        assertThat(gradesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllGrades() throws Exception {
        // Initialize the database
        gradesRepository.saveAndFlush(grades);

        // Get all the gradesList
        restGradesMockMvc.perform(get("/api/grades?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grades.getId().intValue())))
            .andExpect(jsonPath("$.[*].matno").value(hasItem(DEFAULT_MATNO.toString())))
            .andExpect(jsonPath("$.[*].ca").value(hasItem(DEFAULT_CA)))
            .andExpect(jsonPath("$.[*].exam").value(hasItem(DEFAULT_EXAM)))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL)))
            .andExpect(jsonPath("$.[*].grade").value(hasItem(DEFAULT_GRADE.toString())))
            .andExpect(jsonPath("$.[*].semester").value(hasItem(DEFAULT_SEMESTER)))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR.toString())))
            .andExpect(jsonPath("$.[*].attempt").value(hasItem(DEFAULT_ATTEMPT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getGrades() throws Exception {
        // Initialize the database
        gradesRepository.saveAndFlush(grades);

        // Get the grades
        restGradesMockMvc.perform(get("/api/grades/{id}", grades.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(grades.getId().intValue()))
            .andExpect(jsonPath("$.matno").value(DEFAULT_MATNO.toString()))
            .andExpect(jsonPath("$.ca").value(DEFAULT_CA))
            .andExpect(jsonPath("$.exam").value(DEFAULT_EXAM))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL))
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE.toString()))
            .andExpect(jsonPath("$.semester").value(DEFAULT_SEMESTER))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR.toString()))
            .andExpect(jsonPath("$.attempt").value(DEFAULT_ATTEMPT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGrades() throws Exception {
        // Get the grades
        restGradesMockMvc.perform(get("/api/grades/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGrades() throws Exception {
        // Initialize the database
        gradesRepository.saveAndFlush(grades);

        int databaseSizeBeforeUpdate = gradesRepository.findAll().size();

        // Update the grades
        Grades updatedGrades = gradesRepository.findById(grades.getId()).get();
        // Disconnect from session so that the updates on updatedGrades are not directly saved in db
        em.detach(updatedGrades);
        updatedGrades
            .matno(UPDATED_MATNO)
            .ca(UPDATED_CA)
            .exam(UPDATED_EXAM)
            .total(UPDATED_TOTAL)
            .grade(UPDATED_GRADE)
            .semester(UPDATED_SEMESTER)
            .year(UPDATED_YEAR)
            .attempt(UPDATED_ATTEMPT)
            .status(UPDATED_STATUS);
        GradesDTO gradesDTO = gradesMapper.toDto(updatedGrades);

        restGradesMockMvc.perform(put("/api/grades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gradesDTO)))
            .andExpect(status().isOk());

        // Validate the Grades in the database
        List<Grades> gradesList = gradesRepository.findAll();
        assertThat(gradesList).hasSize(databaseSizeBeforeUpdate);
        Grades testGrades = gradesList.get(gradesList.size() - 1);
        assertThat(testGrades.getMatno()).isEqualTo(UPDATED_MATNO);
        assertThat(testGrades.getCa()).isEqualTo(UPDATED_CA);
        assertThat(testGrades.getExam()).isEqualTo(UPDATED_EXAM);
        assertThat(testGrades.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testGrades.getGrade()).isEqualTo(UPDATED_GRADE);
        assertThat(testGrades.getSemester()).isEqualTo(UPDATED_SEMESTER);
        assertThat(testGrades.getYear()).isEqualTo(UPDATED_YEAR);
        assertThat(testGrades.getAttempt()).isEqualTo(UPDATED_ATTEMPT);
        assertThat(testGrades.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingGrades() throws Exception {
        int databaseSizeBeforeUpdate = gradesRepository.findAll().size();

        // Create the Grades
        GradesDTO gradesDTO = gradesMapper.toDto(grades);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGradesMockMvc.perform(put("/api/grades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gradesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Grades in the database
        List<Grades> gradesList = gradesRepository.findAll();
        assertThat(gradesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGrades() throws Exception {
        // Initialize the database
        gradesRepository.saveAndFlush(grades);

        int databaseSizeBeforeDelete = gradesRepository.findAll().size();

        // Delete the grades
        restGradesMockMvc.perform(delete("/api/grades/{id}", grades.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Grades> gradesList = gradesRepository.findAll();
        assertThat(gradesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Grades.class);
        Grades grades1 = new Grades();
        grades1.setId(1L);
        Grades grades2 = new Grades();
        grades2.setId(grades1.getId());
        assertThat(grades1).isEqualTo(grades2);
        grades2.setId(2L);
        assertThat(grades1).isNotEqualTo(grades2);
        grades1.setId(null);
        assertThat(grades1).isNotEqualTo(grades2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GradesDTO.class);
        GradesDTO gradesDTO1 = new GradesDTO();
        gradesDTO1.setId(1L);
        GradesDTO gradesDTO2 = new GradesDTO();
        assertThat(gradesDTO1).isNotEqualTo(gradesDTO2);
        gradesDTO2.setId(gradesDTO1.getId());
        assertThat(gradesDTO1).isEqualTo(gradesDTO2);
        gradesDTO2.setId(2L);
        assertThat(gradesDTO1).isNotEqualTo(gradesDTO2);
        gradesDTO1.setId(null);
        assertThat(gradesDTO1).isNotEqualTo(gradesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(gradesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(gradesMapper.fromId(null)).isNull();
    }
}
