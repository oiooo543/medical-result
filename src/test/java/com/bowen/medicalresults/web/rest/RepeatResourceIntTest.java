package com.bowen.medicalresults.web.rest;

import com.bowen.medicalresults.MedicalResultsApp;

import com.bowen.medicalresults.domain.Repeat;
import com.bowen.medicalresults.repository.RepeatRepository;
import com.bowen.medicalresults.service.RepeatService;
import com.bowen.medicalresults.service.dto.RepeatDTO;
import com.bowen.medicalresults.service.mapper.RepeatMapper;
import com.bowen.medicalresults.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.bowen.medicalresults.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RepeatResource REST controller.
 *
 * @see RepeatResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MedicalResultsApp.class)
public class RepeatResourceIntTest {

    private static final String DEFAULT_MATNO = "AAAAAAAAAA";
    private static final String UPDATED_MATNO = "BBBBBBBBBB";

    private static final String DEFAULT_YEAR = "AAAAAAAAAA";
    private static final String UPDATED_YEAR = "BBBBBBBBBB";

    @Autowired
    private RepeatRepository repeatRepository;

    @Autowired
    private RepeatMapper repeatMapper;

    @Autowired
    private RepeatService repeatService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRepeatMockMvc;

    private Repeat repeat;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RepeatResource repeatResource = new RepeatResource(repeatService);
        this.restRepeatMockMvc = MockMvcBuilders.standaloneSetup(repeatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Repeat createEntity(EntityManager em) {
        Repeat repeat = new Repeat()
            .matno(DEFAULT_MATNO)
            .year(DEFAULT_YEAR);
        return repeat;
    }

    @Before
    public void initTest() {
        repeat = createEntity(em);
    }

    @Test
    @Transactional
    public void createRepeat() throws Exception {
        int databaseSizeBeforeCreate = repeatRepository.findAll().size();

        // Create the Repeat
        RepeatDTO repeatDTO = repeatMapper.toDto(repeat);
        restRepeatMockMvc.perform(post("/api/repeats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(repeatDTO)))
            .andExpect(status().isCreated());

        // Validate the Repeat in the database
        List<Repeat> repeatList = repeatRepository.findAll();
        assertThat(repeatList).hasSize(databaseSizeBeforeCreate + 1);
        Repeat testRepeat = repeatList.get(repeatList.size() - 1);
        assertThat(testRepeat.getMatno()).isEqualTo(DEFAULT_MATNO);
        assertThat(testRepeat.getYear()).isEqualTo(DEFAULT_YEAR);
    }

    @Test
    @Transactional
    public void createRepeatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = repeatRepository.findAll().size();

        // Create the Repeat with an existing ID
        repeat.setId(1L);
        RepeatDTO repeatDTO = repeatMapper.toDto(repeat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRepeatMockMvc.perform(post("/api/repeats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(repeatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Repeat in the database
        List<Repeat> repeatList = repeatRepository.findAll();
        assertThat(repeatList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRepeats() throws Exception {
        // Initialize the database
        repeatRepository.saveAndFlush(repeat);

        // Get all the repeatList
        restRepeatMockMvc.perform(get("/api/repeats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(repeat.getId().intValue())))
            .andExpect(jsonPath("$.[*].matno").value(hasItem(DEFAULT_MATNO.toString())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR.toString())));
    }
    
    @Test
    @Transactional
    public void getRepeat() throws Exception {
        // Initialize the database
        repeatRepository.saveAndFlush(repeat);

        // Get the repeat
        restRepeatMockMvc.perform(get("/api/repeats/{id}", repeat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(repeat.getId().intValue()))
            .andExpect(jsonPath("$.matno").value(DEFAULT_MATNO.toString()))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRepeat() throws Exception {
        // Get the repeat
        restRepeatMockMvc.perform(get("/api/repeats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRepeat() throws Exception {
        // Initialize the database
        repeatRepository.saveAndFlush(repeat);

        int databaseSizeBeforeUpdate = repeatRepository.findAll().size();

        // Update the repeat
        Repeat updatedRepeat = repeatRepository.findById(repeat.getId()).get();
        // Disconnect from session so that the updates on updatedRepeat are not directly saved in db
        em.detach(updatedRepeat);
        updatedRepeat
            .matno(UPDATED_MATNO)
            .year(UPDATED_YEAR);
        RepeatDTO repeatDTO = repeatMapper.toDto(updatedRepeat);

        restRepeatMockMvc.perform(put("/api/repeats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(repeatDTO)))
            .andExpect(status().isOk());

        // Validate the Repeat in the database
        List<Repeat> repeatList = repeatRepository.findAll();
        assertThat(repeatList).hasSize(databaseSizeBeforeUpdate);
        Repeat testRepeat = repeatList.get(repeatList.size() - 1);
        assertThat(testRepeat.getMatno()).isEqualTo(UPDATED_MATNO);
        assertThat(testRepeat.getYear()).isEqualTo(UPDATED_YEAR);
    }

    @Test
    @Transactional
    public void updateNonExistingRepeat() throws Exception {
        int databaseSizeBeforeUpdate = repeatRepository.findAll().size();

        // Create the Repeat
        RepeatDTO repeatDTO = repeatMapper.toDto(repeat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRepeatMockMvc.perform(put("/api/repeats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(repeatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Repeat in the database
        List<Repeat> repeatList = repeatRepository.findAll();
        assertThat(repeatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRepeat() throws Exception {
        // Initialize the database
        repeatRepository.saveAndFlush(repeat);

        int databaseSizeBeforeDelete = repeatRepository.findAll().size();

        // Delete the repeat
        restRepeatMockMvc.perform(delete("/api/repeats/{id}", repeat.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Repeat> repeatList = repeatRepository.findAll();
        assertThat(repeatList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Repeat.class);
        Repeat repeat1 = new Repeat();
        repeat1.setId(1L);
        Repeat repeat2 = new Repeat();
        repeat2.setId(repeat1.getId());
        assertThat(repeat1).isEqualTo(repeat2);
        repeat2.setId(2L);
        assertThat(repeat1).isNotEqualTo(repeat2);
        repeat1.setId(null);
        assertThat(repeat1).isNotEqualTo(repeat2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RepeatDTO.class);
        RepeatDTO repeatDTO1 = new RepeatDTO();
        repeatDTO1.setId(1L);
        RepeatDTO repeatDTO2 = new RepeatDTO();
        assertThat(repeatDTO1).isNotEqualTo(repeatDTO2);
        repeatDTO2.setId(repeatDTO1.getId());
        assertThat(repeatDTO1).isEqualTo(repeatDTO2);
        repeatDTO2.setId(2L);
        assertThat(repeatDTO1).isNotEqualTo(repeatDTO2);
        repeatDTO1.setId(null);
        assertThat(repeatDTO1).isNotEqualTo(repeatDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(repeatMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(repeatMapper.fromId(null)).isNull();
    }
}
