/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { MedicalResultsTestModule } from '../../../test.module';
import { RepeatUpdateComponent } from 'app/entities/repeat/repeat-update.component';
import { RepeatService } from 'app/entities/repeat/repeat.service';
import { Repeat } from 'app/shared/model/repeat.model';

describe('Component Tests', () => {
    describe('Repeat Management Update Component', () => {
        let comp: RepeatUpdateComponent;
        let fixture: ComponentFixture<RepeatUpdateComponent>;
        let service: RepeatService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MedicalResultsTestModule],
                declarations: [RepeatUpdateComponent]
            })
                .overrideTemplate(RepeatUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RepeatUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RepeatService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Repeat(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.repeat = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Repeat();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.repeat = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
