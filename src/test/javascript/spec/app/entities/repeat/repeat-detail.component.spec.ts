/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MedicalResultsTestModule } from '../../../test.module';
import { RepeatDetailComponent } from 'app/entities/repeat/repeat-detail.component';
import { Repeat } from 'app/shared/model/repeat.model';

describe('Component Tests', () => {
    describe('Repeat Management Detail Component', () => {
        let comp: RepeatDetailComponent;
        let fixture: ComponentFixture<RepeatDetailComponent>;
        const route = ({ data: of({ repeat: new Repeat(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MedicalResultsTestModule],
                declarations: [RepeatDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RepeatDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RepeatDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.repeat).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
