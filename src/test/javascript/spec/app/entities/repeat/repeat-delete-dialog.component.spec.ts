/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MedicalResultsTestModule } from '../../../test.module';
import { RepeatDeleteDialogComponent } from 'app/entities/repeat/repeat-delete-dialog.component';
import { RepeatService } from 'app/entities/repeat/repeat.service';

describe('Component Tests', () => {
    describe('Repeat Management Delete Component', () => {
        let comp: RepeatDeleteDialogComponent;
        let fixture: ComponentFixture<RepeatDeleteDialogComponent>;
        let service: RepeatService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MedicalResultsTestModule],
                declarations: [RepeatDeleteDialogComponent]
            })
                .overrideTemplate(RepeatDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RepeatDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RepeatService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
