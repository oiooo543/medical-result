/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MedicalResultsTestModule } from '../../../test.module';
import { RepeatComponent } from 'app/entities/repeat/repeat.component';
import { RepeatService } from 'app/entities/repeat/repeat.service';
import { Repeat } from 'app/shared/model/repeat.model';

describe('Component Tests', () => {
    describe('Repeat Management Component', () => {
        let comp: RepeatComponent;
        let fixture: ComponentFixture<RepeatComponent>;
        let service: RepeatService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MedicalResultsTestModule],
                declarations: [RepeatComponent],
                providers: []
            })
                .overrideTemplate(RepeatComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RepeatComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RepeatService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Repeat(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.repeats[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
