/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { GradesService } from 'app/entities/grades/grades.service';
import { IGrades, Grades } from 'app/shared/model/grades.model';

describe('Service Tests', () => {
    describe('Grades Service', () => {
        let injector: TestBed;
        let service: GradesService;
        let httpMock: HttpTestingController;
        let elemDefault: IGrades;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(GradesService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new Grades(0, 'AAAAAAA', 0, 0, 0, 'AAAAAAA', 0, 'AAAAAAA', 0, false);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Grades', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new Grades(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Grades', async () => {
                const returnedFromService = Object.assign(
                    {
                        matno: 'BBBBBB',
                        ca: 1,
                        exam: 1,
                        total: 1,
                        grade: 'BBBBBB',
                        semester: 1,
                        year: 'BBBBBB',
                        attempt: 1,
                        status: true
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Grades', async () => {
                const returnedFromService = Object.assign(
                    {
                        matno: 'BBBBBB',
                        ca: 1,
                        exam: 1,
                        total: 1,
                        grade: 'BBBBBB',
                        semester: 1,
                        year: 'BBBBBB',
                        attempt: 1,
                        status: true
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Grades', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
