import { element, by, ElementFinder } from 'protractor';

export class GradesComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-grades div table .btn-danger'));
    title = element.all(by.css('jhi-grades div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class GradesUpdatePage {
    pageTitle = element(by.id('jhi-grades-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    matnoInput = element(by.id('field_matno'));
    caInput = element(by.id('field_ca'));
    examInput = element(by.id('field_exam'));
    totalInput = element(by.id('field_total'));
    gradeInput = element(by.id('field_grade'));
    semesterInput = element(by.id('field_semester'));
    yearInput = element(by.id('field_year'));
    attemptInput = element(by.id('field_attempt'));
    statusInput = element(by.id('field_status'));
    courseSelect = element(by.id('field_course'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setMatnoInput(matno) {
        await this.matnoInput.sendKeys(matno);
    }

    async getMatnoInput() {
        return this.matnoInput.getAttribute('value');
    }

    async setCaInput(ca) {
        await this.caInput.sendKeys(ca);
    }

    async getCaInput() {
        return this.caInput.getAttribute('value');
    }

    async setExamInput(exam) {
        await this.examInput.sendKeys(exam);
    }

    async getExamInput() {
        return this.examInput.getAttribute('value');
    }

    async setTotalInput(total) {
        await this.totalInput.sendKeys(total);
    }

    async getTotalInput() {
        return this.totalInput.getAttribute('value');
    }

    async setGradeInput(grade) {
        await this.gradeInput.sendKeys(grade);
    }

    async getGradeInput() {
        return this.gradeInput.getAttribute('value');
    }

    async setSemesterInput(semester) {
        await this.semesterInput.sendKeys(semester);
    }

    async getSemesterInput() {
        return this.semesterInput.getAttribute('value');
    }

    async setYearInput(year) {
        await this.yearInput.sendKeys(year);
    }

    async getYearInput() {
        return this.yearInput.getAttribute('value');
    }

    async setAttemptInput(attempt) {
        await this.attemptInput.sendKeys(attempt);
    }

    async getAttemptInput() {
        return this.attemptInput.getAttribute('value');
    }

    getStatusInput() {
        return this.statusInput;
    }

    async courseSelectLastOption() {
        await this.courseSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async courseSelectOption(option) {
        await this.courseSelect.sendKeys(option);
    }

    getCourseSelect(): ElementFinder {
        return this.courseSelect;
    }

    async getCourseSelectedOption() {
        return this.courseSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class GradesDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-grades-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-grades'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
