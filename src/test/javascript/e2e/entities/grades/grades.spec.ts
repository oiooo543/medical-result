/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { GradesComponentsPage, GradesDeleteDialog, GradesUpdatePage } from './grades.page-object';

const expect = chai.expect;

describe('Grades e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let gradesUpdatePage: GradesUpdatePage;
    let gradesComponentsPage: GradesComponentsPage;
    let gradesDeleteDialog: GradesDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Grades', async () => {
        await navBarPage.goToEntity('grades');
        gradesComponentsPage = new GradesComponentsPage();
        await browser.wait(ec.visibilityOf(gradesComponentsPage.title), 5000);
        expect(await gradesComponentsPage.getTitle()).to.eq('Grades');
    });

    it('should load create Grades page', async () => {
        await gradesComponentsPage.clickOnCreateButton();
        gradesUpdatePage = new GradesUpdatePage();
        expect(await gradesUpdatePage.getPageTitle()).to.eq('Create or edit a Grades');
        await gradesUpdatePage.cancel();
    });

    it('should create and save Grades', async () => {
        const nbButtonsBeforeCreate = await gradesComponentsPage.countDeleteButtons();

        await gradesComponentsPage.clickOnCreateButton();
        await promise.all([
            gradesUpdatePage.setMatnoInput('matno'),
            gradesUpdatePage.setCaInput('5'),
            gradesUpdatePage.setExamInput('5'),
            gradesUpdatePage.setTotalInput('5'),
            gradesUpdatePage.setGradeInput('grade'),
            gradesUpdatePage.setSemesterInput('5'),
            gradesUpdatePage.setYearInput('year'),
            gradesUpdatePage.setAttemptInput('5'),
            gradesUpdatePage.courseSelectLastOption()
        ]);
        expect(await gradesUpdatePage.getMatnoInput()).to.eq('matno');
        expect(await gradesUpdatePage.getCaInput()).to.eq('5');
        expect(await gradesUpdatePage.getExamInput()).to.eq('5');
        expect(await gradesUpdatePage.getTotalInput()).to.eq('5');
        expect(await gradesUpdatePage.getGradeInput()).to.eq('grade');
        expect(await gradesUpdatePage.getSemesterInput()).to.eq('5');
        expect(await gradesUpdatePage.getYearInput()).to.eq('year');
        expect(await gradesUpdatePage.getAttemptInput()).to.eq('5');
        const selectedStatus = gradesUpdatePage.getStatusInput();
        if (await selectedStatus.isSelected()) {
            await gradesUpdatePage.getStatusInput().click();
            expect(await gradesUpdatePage.getStatusInput().isSelected()).to.be.false;
        } else {
            await gradesUpdatePage.getStatusInput().click();
            expect(await gradesUpdatePage.getStatusInput().isSelected()).to.be.true;
        }
        await gradesUpdatePage.save();
        expect(await gradesUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await gradesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Grades', async () => {
        const nbButtonsBeforeDelete = await gradesComponentsPage.countDeleteButtons();
        await gradesComponentsPage.clickOnLastDeleteButton();

        gradesDeleteDialog = new GradesDeleteDialog();
        expect(await gradesDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Grades?');
        await gradesDeleteDialog.clickOnConfirmButton();

        expect(await gradesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
