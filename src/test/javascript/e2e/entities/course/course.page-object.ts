import { element, by, ElementFinder } from 'protractor';

export class CourseComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-course div table .btn-danger'));
    title = element.all(by.css('jhi-course div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class CourseUpdatePage {
    pageTitle = element(by.id('jhi-course-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nameInput = element(by.id('field_name'));
    codeInput = element(by.id('field_code'));
    levelInput = element(by.id('field_level'));
    creditInput = element(by.id('field_credit'));
    localeInput = element(by.id('field_locale'));
    statusInput = element(by.id('field_status'));
    departmentSelect = element(by.id('field_department'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNameInput(name) {
        await this.nameInput.sendKeys(name);
    }

    async getNameInput() {
        return this.nameInput.getAttribute('value');
    }

    async setCodeInput(code) {
        await this.codeInput.sendKeys(code);
    }

    async getCodeInput() {
        return this.codeInput.getAttribute('value');
    }

    async setLevelInput(level) {
        await this.levelInput.sendKeys(level);
    }

    async getLevelInput() {
        return this.levelInput.getAttribute('value');
    }

    async setCreditInput(credit) {
        await this.creditInput.sendKeys(credit);
    }

    async getCreditInput() {
        return this.creditInput.getAttribute('value');
    }

    async setLocaleInput(locale) {
        await this.localeInput.sendKeys(locale);
    }

    async getLocaleInput() {
        return this.localeInput.getAttribute('value');
    }

    async setStatusInput(status) {
        await this.statusInput.sendKeys(status);
    }

    async getStatusInput() {
        return this.statusInput.getAttribute('value');
    }

    async departmentSelectLastOption() {
        await this.departmentSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async departmentSelectOption(option) {
        await this.departmentSelect.sendKeys(option);
    }

    getDepartmentSelect(): ElementFinder {
        return this.departmentSelect;
    }

    async getDepartmentSelectedOption() {
        return this.departmentSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class CourseDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-course-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-course'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
