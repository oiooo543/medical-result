/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FacultyComponentsPage, FacultyDeleteDialog, FacultyUpdatePage } from './faculty.page-object';

const expect = chai.expect;

describe('Faculty e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let facultyUpdatePage: FacultyUpdatePage;
    let facultyComponentsPage: FacultyComponentsPage;
    let facultyDeleteDialog: FacultyDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Faculties', async () => {
        await navBarPage.goToEntity('faculty');
        facultyComponentsPage = new FacultyComponentsPage();
        await browser.wait(ec.visibilityOf(facultyComponentsPage.title), 5000);
        expect(await facultyComponentsPage.getTitle()).to.eq('Faculties');
    });

    it('should load create Faculty page', async () => {
        await facultyComponentsPage.clickOnCreateButton();
        facultyUpdatePage = new FacultyUpdatePage();
        expect(await facultyUpdatePage.getPageTitle()).to.eq('Create or edit a Faculty');
        await facultyUpdatePage.cancel();
    });

    it('should create and save Faculties', async () => {
        const nbButtonsBeforeCreate = await facultyComponentsPage.countDeleteButtons();

        await facultyComponentsPage.clickOnCreateButton();
        await promise.all([facultyUpdatePage.setNameInput('name')]);
        expect(await facultyUpdatePage.getNameInput()).to.eq('name');
        await facultyUpdatePage.save();
        expect(await facultyUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await facultyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Faculty', async () => {
        const nbButtonsBeforeDelete = await facultyComponentsPage.countDeleteButtons();
        await facultyComponentsPage.clickOnLastDeleteButton();

        facultyDeleteDialog = new FacultyDeleteDialog();
        expect(await facultyDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Faculty?');
        await facultyDeleteDialog.clickOnConfirmButton();

        expect(await facultyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
