import { element, by, ElementFinder } from 'protractor';

export class RepeatComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-repeat div table .btn-danger'));
    title = element.all(by.css('jhi-repeat div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class RepeatUpdatePage {
    pageTitle = element(by.id('jhi-repeat-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    matnoInput = element(by.id('field_matno'));
    yearInput = element(by.id('field_year'));
    studentSelect = element(by.id('field_student'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setMatnoInput(matno) {
        await this.matnoInput.sendKeys(matno);
    }

    async getMatnoInput() {
        return this.matnoInput.getAttribute('value');
    }

    async setYearInput(year) {
        await this.yearInput.sendKeys(year);
    }

    async getYearInput() {
        return this.yearInput.getAttribute('value');
    }

    async studentSelectLastOption() {
        await this.studentSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async studentSelectOption(option) {
        await this.studentSelect.sendKeys(option);
    }

    getStudentSelect(): ElementFinder {
        return this.studentSelect;
    }

    async getStudentSelectedOption() {
        return this.studentSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class RepeatDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-repeat-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-repeat'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
