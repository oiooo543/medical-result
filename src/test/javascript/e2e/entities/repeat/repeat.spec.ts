/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { RepeatComponentsPage, RepeatDeleteDialog, RepeatUpdatePage } from './repeat.page-object';

const expect = chai.expect;

describe('Repeat e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let repeatUpdatePage: RepeatUpdatePage;
    let repeatComponentsPage: RepeatComponentsPage;
    let repeatDeleteDialog: RepeatDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Repeats', async () => {
        await navBarPage.goToEntity('repeat');
        repeatComponentsPage = new RepeatComponentsPage();
        await browser.wait(ec.visibilityOf(repeatComponentsPage.title), 5000);
        expect(await repeatComponentsPage.getTitle()).to.eq('Repeats');
    });

    it('should load create Repeat page', async () => {
        await repeatComponentsPage.clickOnCreateButton();
        repeatUpdatePage = new RepeatUpdatePage();
        expect(await repeatUpdatePage.getPageTitle()).to.eq('Create or edit a Repeat');
        await repeatUpdatePage.cancel();
    });

    it('should create and save Repeats', async () => {
        const nbButtonsBeforeCreate = await repeatComponentsPage.countDeleteButtons();

        await repeatComponentsPage.clickOnCreateButton();
        await promise.all([
            repeatUpdatePage.setMatnoInput('matno'),
            repeatUpdatePage.setYearInput('year'),
            repeatUpdatePage.studentSelectLastOption()
        ]);
        expect(await repeatUpdatePage.getMatnoInput()).to.eq('matno');
        expect(await repeatUpdatePage.getYearInput()).to.eq('year');
        await repeatUpdatePage.save();
        expect(await repeatUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await repeatComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Repeat', async () => {
        const nbButtonsBeforeDelete = await repeatComponentsPage.countDeleteButtons();
        await repeatComponentsPage.clickOnLastDeleteButton();

        repeatDeleteDialog = new RepeatDeleteDialog();
        expect(await repeatDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Repeat?');
        await repeatDeleteDialog.clickOnConfirmButton();

        expect(await repeatComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
