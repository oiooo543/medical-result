import { IGrades } from 'app/shared/model/grades.model';

export interface ICourse {
    id?: number;
    name?: string;
    code?: string;
    level?: number;
    credit?: number;
    locale?: string;
    status?: string;
    results?: IGrades[];
    departmentId?: number;
}

export class Course implements ICourse {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
        public level?: number,
        public credit?: number,
        public locale?: string,
        public status?: string,
        public results?: IGrades[],
        public departmentId?: number
    ) {}
}
