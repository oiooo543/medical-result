import { IStudent } from 'app/shared/model/student.model';
import { ICourse } from 'app/shared/model/course.model';

export interface IDepartment {
    id?: number;
    name?: string;
    students?: IStudent[];
    courses?: ICourse[];
    facultyId?: number;
}

export class Department implements IDepartment {
    constructor(
        public id?: number,
        public name?: string,
        public students?: IStudent[],
        public courses?: ICourse[],
        public facultyId?: number
    ) {}
}
