export interface IRepeat {
    id?: number;
    matno?: string;
    year?: string;
    studentId?: number;
}

export class Repeat implements IRepeat {
    constructor(public id?: number, public matno?: string, public year?: string, public studentId?: number) {}
}
