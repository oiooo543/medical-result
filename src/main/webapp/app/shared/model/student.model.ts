export interface IStudent {
    id?: number;
    matno?: string;
    level?: number;
    userId?: number;
    departmentId?: number;
}

export class Student implements IStudent {
    constructor(public id?: number, public matno?: string, public level?: number, public userId?: number, public departmentId?: number) {}
}
