export interface IGrades {
    id?: number;
    matno?: string;
    ca?: number;
    exam?: number;
    total?: number;
    grade?: string;
    semester?: number;
    year?: string;
    attempt?: number;
    status?: boolean;
    courseId?: number;
}

export class Grades implements IGrades {
    constructor(
        public id?: number,
        public matno?: string,
        public ca?: number,
        public exam?: number,
        public total?: number,
        public grade?: string,
        public semester?: number,
        public year?: string,
        public attempt?: number,
        public status?: boolean,
        public courseId?: number
    ) {
        this.status = this.status || false;
    }
}
