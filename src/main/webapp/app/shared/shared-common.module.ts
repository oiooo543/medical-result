import { NgModule } from '@angular/core';

import { MedicalResultsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [MedicalResultsSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [MedicalResultsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class MedicalResultsSharedCommonModule {}
