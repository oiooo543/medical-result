import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRepeat } from 'app/shared/model/repeat.model';

type EntityResponseType = HttpResponse<IRepeat>;
type EntityArrayResponseType = HttpResponse<IRepeat[]>;

@Injectable({ providedIn: 'root' })
export class RepeatService {
    public resourceUrl = SERVER_API_URL + 'api/repeats';

    constructor(protected http: HttpClient) {}

    create(repeat: IRepeat): Observable<EntityResponseType> {
        return this.http.post<IRepeat>(this.resourceUrl, repeat, { observe: 'response' });
    }

    update(repeat: IRepeat): Observable<EntityResponseType> {
        return this.http.put<IRepeat>(this.resourceUrl, repeat, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRepeat>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRepeat[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
