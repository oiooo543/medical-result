import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MedicalResultsSharedModule } from 'app/shared';
import {
    RepeatComponent,
    RepeatDetailComponent,
    RepeatUpdateComponent,
    RepeatDeletePopupComponent,
    RepeatDeleteDialogComponent,
    repeatRoute,
    repeatPopupRoute
} from './';

const ENTITY_STATES = [...repeatRoute, ...repeatPopupRoute];

@NgModule({
    imports: [MedicalResultsSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [RepeatComponent, RepeatDetailComponent, RepeatUpdateComponent, RepeatDeleteDialogComponent, RepeatDeletePopupComponent],
    entryComponents: [RepeatComponent, RepeatUpdateComponent, RepeatDeleteDialogComponent, RepeatDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MedicalResultsRepeatModule {}
