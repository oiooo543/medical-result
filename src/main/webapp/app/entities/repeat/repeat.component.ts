import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRepeat } from 'app/shared/model/repeat.model';
import { AccountService } from 'app/core';
import { RepeatService } from './repeat.service';

@Component({
    selector: 'jhi-repeat',
    templateUrl: './repeat.component.html'
})
export class RepeatComponent implements OnInit, OnDestroy {
    repeats: IRepeat[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected repeatService: RepeatService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.repeatService
            .query()
            .pipe(
                filter((res: HttpResponse<IRepeat[]>) => res.ok),
                map((res: HttpResponse<IRepeat[]>) => res.body)
            )
            .subscribe(
                (res: IRepeat[]) => {
                    this.repeats = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRepeats();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRepeat) {
        return item.id;
    }

    registerChangeInRepeats() {
        this.eventSubscriber = this.eventManager.subscribe('repeatListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
