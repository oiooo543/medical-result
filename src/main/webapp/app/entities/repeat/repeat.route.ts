import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Repeat } from 'app/shared/model/repeat.model';
import { RepeatService } from './repeat.service';
import { RepeatComponent } from './repeat.component';
import { RepeatDetailComponent } from './repeat-detail.component';
import { RepeatUpdateComponent } from './repeat-update.component';
import { RepeatDeletePopupComponent } from './repeat-delete-dialog.component';
import { IRepeat } from 'app/shared/model/repeat.model';

@Injectable({ providedIn: 'root' })
export class RepeatResolve implements Resolve<IRepeat> {
    constructor(private service: RepeatService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRepeat> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Repeat>) => response.ok),
                map((repeat: HttpResponse<Repeat>) => repeat.body)
            );
        }
        return of(new Repeat());
    }
}

export const repeatRoute: Routes = [
    {
        path: '',
        component: RepeatComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Repeats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: RepeatDetailComponent,
        resolve: {
            repeat: RepeatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Repeats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: RepeatUpdateComponent,
        resolve: {
            repeat: RepeatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Repeats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: RepeatUpdateComponent,
        resolve: {
            repeat: RepeatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Repeats'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const repeatPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: RepeatDeletePopupComponent,
        resolve: {
            repeat: RepeatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Repeats'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
