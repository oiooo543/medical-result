import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRepeat } from 'app/shared/model/repeat.model';
import { RepeatService } from './repeat.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student';

@Component({
    selector: 'jhi-repeat-update',
    templateUrl: './repeat-update.component.html'
})
export class RepeatUpdateComponent implements OnInit {
    repeat: IRepeat;
    isSaving: boolean;

    students: IStudent[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected repeatService: RepeatService,
        protected studentService: StudentService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ repeat }) => {
            this.repeat = repeat;
        });
        this.studentService
            .query({ filter: 'repeat-is-null' })
            .pipe(
                filter((mayBeOk: HttpResponse<IStudent[]>) => mayBeOk.ok),
                map((response: HttpResponse<IStudent[]>) => response.body)
            )
            .subscribe(
                (res: IStudent[]) => {
                    if (!this.repeat.studentId) {
                        this.students = res;
                    } else {
                        this.studentService
                            .find(this.repeat.studentId)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<IStudent>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<IStudent>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: IStudent) => (this.students = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.repeat.id !== undefined) {
            this.subscribeToSaveResponse(this.repeatService.update(this.repeat));
        } else {
            this.subscribeToSaveResponse(this.repeatService.create(this.repeat));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IRepeat>>) {
        result.subscribe((res: HttpResponse<IRepeat>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackStudentById(index: number, item: IStudent) {
        return item.id;
    }
}
