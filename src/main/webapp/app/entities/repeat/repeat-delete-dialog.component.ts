import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRepeat } from 'app/shared/model/repeat.model';
import { RepeatService } from './repeat.service';

@Component({
    selector: 'jhi-repeat-delete-dialog',
    templateUrl: './repeat-delete-dialog.component.html'
})
export class RepeatDeleteDialogComponent {
    repeat: IRepeat;

    constructor(protected repeatService: RepeatService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.repeatService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'repeatListModification',
                content: 'Deleted an repeat'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-repeat-delete-popup',
    template: ''
})
export class RepeatDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ repeat }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(RepeatDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.repeat = repeat;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/repeat', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/repeat', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
