import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRepeat } from 'app/shared/model/repeat.model';

@Component({
    selector: 'jhi-repeat-detail',
    templateUrl: './repeat-detail.component.html'
})
export class RepeatDetailComponent implements OnInit {
    repeat: IRepeat;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ repeat }) => {
            this.repeat = repeat;
        });
    }

    previousState() {
        window.history.back();
    }
}
