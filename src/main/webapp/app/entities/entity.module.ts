import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'faculty',
                loadChildren: './faculty/faculty.module#MedicalResultsFacultyModule'
            },
            {
                path: 'department',
                loadChildren: './department/department.module#MedicalResultsDepartmentModule'
            },
            {
                path: 'student',
                loadChildren: './student/student.module#MedicalResultsStudentModule'
            },
            {
                path: 'course',
                loadChildren: './course/course.module#MedicalResultsCourseModule'
            },
            {
                path: 'grades',
                loadChildren: './grades/grades.module#MedicalResultsGradesModule'
            },
            {
                path: 'repeat',
                loadChildren: './repeat/repeat.module#MedicalResultsRepeatModule'
            },
            {
                path: 'student',
                loadChildren: './student/student.module#MedicalResultsStudentModule'
            },
            {
                path: 'repeat',
                loadChildren: './repeat/repeat.module#MedicalResultsRepeatModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MedicalResultsEntityModule {}
