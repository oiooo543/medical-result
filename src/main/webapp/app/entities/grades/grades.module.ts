import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MedicalResultsSharedModule } from 'app/shared';
import {
    GradesComponent,
    GradesDetailComponent,
    GradesUpdateComponent,
    GradesDeletePopupComponent,
    GradesDeleteDialogComponent,
    gradesRoute,
    gradesPopupRoute
} from './';

const ENTITY_STATES = [...gradesRoute, ...gradesPopupRoute];

@NgModule({
    imports: [MedicalResultsSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [GradesComponent, GradesDetailComponent, GradesUpdateComponent, GradesDeleteDialogComponent, GradesDeletePopupComponent],
    entryComponents: [GradesComponent, GradesUpdateComponent, GradesDeleteDialogComponent, GradesDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MedicalResultsGradesModule {}
