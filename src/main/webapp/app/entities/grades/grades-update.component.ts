import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IGrades } from 'app/shared/model/grades.model';
import { GradesService } from './grades.service';
import { ICourse } from 'app/shared/model/course.model';
import { CourseService } from 'app/entities/course';

@Component({
    selector: 'jhi-grades-update',
    templateUrl: './grades-update.component.html'
})
export class GradesUpdateComponent implements OnInit {
    grades: IGrades;
    isSaving: boolean;

    courses: ICourse[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected gradesService: GradesService,
        protected courseService: CourseService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ grades }) => {
            this.grades = grades;
        });
        this.courseService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICourse[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICourse[]>) => response.body)
            )
            .subscribe((res: ICourse[]) => (this.courses = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.grades.id !== undefined) {
            this.subscribeToSaveResponse(this.gradesService.update(this.grades));
        } else {
            this.subscribeToSaveResponse(this.gradesService.create(this.grades));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IGrades>>) {
        result.subscribe((res: HttpResponse<IGrades>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCourseById(index: number, item: ICourse) {
        return item.id;
    }
}
