package com.bowen.medicalresults.web.rest;
import com.bowen.medicalresults.service.GradesService;
import com.bowen.medicalresults.web.rest.errors.BadRequestAlertException;
import com.bowen.medicalresults.web.rest.util.HeaderUtil;
import com.bowen.medicalresults.service.dto.GradesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Grades.
 */
@RestController
@RequestMapping("/api")
public class GradesResource {

    private final Logger log = LoggerFactory.getLogger(GradesResource.class);

    private static final String ENTITY_NAME = "grades";

    private final GradesService gradesService;

    public GradesResource(GradesService gradesService) {
        this.gradesService = gradesService;
    }

    /**
     * POST  /grades : Create a new grades.
     *
     * @param gradesDTO the gradesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gradesDTO, or with status 400 (Bad Request) if the grades has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/grades")
    public ResponseEntity<GradesDTO> createGrades(@RequestBody GradesDTO gradesDTO) throws URISyntaxException {
        log.debug("REST request to save Grades : {}", gradesDTO);
        if (gradesDTO.getId() != null) {
            throw new BadRequestAlertException("A new grades cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GradesDTO result = gradesService.save(gradesDTO);
        return ResponseEntity.created(new URI("/api/grades/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /grades : Updates an existing grades.
     *
     * @param gradesDTO the gradesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gradesDTO,
     * or with status 400 (Bad Request) if the gradesDTO is not valid,
     * or with status 500 (Internal Server Error) if the gradesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/grades")
    public ResponseEntity<GradesDTO> updateGrades(@RequestBody GradesDTO gradesDTO) throws URISyntaxException {
        log.debug("REST request to update Grades : {}", gradesDTO);
        if (gradesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GradesDTO result = gradesService.save(gradesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gradesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /grades : get all the grades.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of grades in body
     */
    @GetMapping("/grades")
    public List<GradesDTO> getAllGrades() {
        log.debug("REST request to get all Grades");
        return gradesService.findAll();
    }

    /**
     * GET  /grades/:id : get the "id" grades.
     *
     * @param id the id of the gradesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gradesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/grades/{id}")
    public ResponseEntity<GradesDTO> getGrades(@PathVariable Long id) {
        log.debug("REST request to get Grades : {}", id);
        Optional<GradesDTO> gradesDTO = gradesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(gradesDTO);
    }

    /**
     * DELETE  /grades/:id : delete the "id" grades.
     *
     * @param id the id of the gradesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/grades/{id}")
    public ResponseEntity<Void> deleteGrades(@PathVariable Long id) {
        log.debug("REST request to delete Grades : {}", id);
        gradesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
