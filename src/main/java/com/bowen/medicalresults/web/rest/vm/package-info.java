/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bowen.medicalresults.web.rest.vm;
