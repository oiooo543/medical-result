package com.bowen.medicalresults.web.rest;
import com.bowen.medicalresults.service.RepeatService;
import com.bowen.medicalresults.web.rest.errors.BadRequestAlertException;
import com.bowen.medicalresults.web.rest.util.HeaderUtil;
import com.bowen.medicalresults.service.dto.RepeatDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Repeat.
 */
@RestController
@RequestMapping("/api")
public class RepeatResource {

    private final Logger log = LoggerFactory.getLogger(RepeatResource.class);

    private static final String ENTITY_NAME = "repeat";

    private final RepeatService repeatService;

    public RepeatResource(RepeatService repeatService) {
        this.repeatService = repeatService;
    }

    /**
     * POST  /repeats : Create a new repeat.
     *
     * @param repeatDTO the repeatDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new repeatDTO, or with status 400 (Bad Request) if the repeat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/repeats")
    public ResponseEntity<RepeatDTO> createRepeat(@RequestBody RepeatDTO repeatDTO) throws URISyntaxException {
        log.debug("REST request to save Repeat : {}", repeatDTO);
        if (repeatDTO.getId() != null) {
            throw new BadRequestAlertException("A new repeat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RepeatDTO result = repeatService.save(repeatDTO);
        return ResponseEntity.created(new URI("/api/repeats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /repeats : Updates an existing repeat.
     *
     * @param repeatDTO the repeatDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated repeatDTO,
     * or with status 400 (Bad Request) if the repeatDTO is not valid,
     * or with status 500 (Internal Server Error) if the repeatDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/repeats")
    public ResponseEntity<RepeatDTO> updateRepeat(@RequestBody RepeatDTO repeatDTO) throws URISyntaxException {
        log.debug("REST request to update Repeat : {}", repeatDTO);
        if (repeatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RepeatDTO result = repeatService.save(repeatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, repeatDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /repeats : get all the repeats.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of repeats in body
     */
    @GetMapping("/repeats")
    public List<RepeatDTO> getAllRepeats() {
        log.debug("REST request to get all Repeats");
        return repeatService.findAll();
    }

    /**
     * GET  /repeats/:id : get the "id" repeat.
     *
     * @param id the id of the repeatDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the repeatDTO, or with status 404 (Not Found)
     */
    @GetMapping("/repeats/{id}")
    public ResponseEntity<RepeatDTO> getRepeat(@PathVariable Long id) {
        log.debug("REST request to get Repeat : {}", id);
        Optional<RepeatDTO> repeatDTO = repeatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(repeatDTO);
    }

    /**
     * DELETE  /repeats/:id : delete the "id" repeat.
     *
     * @param id the id of the repeatDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/repeats/{id}")
    public ResponseEntity<Void> deleteRepeat(@PathVariable Long id) {
        log.debug("REST request to delete Repeat : {}", id);
        repeatService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
