package com.bowen.medicalresults.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Repeat.
 */
@Entity
@Table(name = "jhi_repeat")
public class Repeat implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "matno")
    private String matno;

    @Column(name = "jhi_year")
    private String year;

    @OneToOne
    @JoinColumn(unique = true)
    private Student student;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatno() {
        return matno;
    }

    public Repeat matno(String matno) {
        this.matno = matno;
        return this;
    }

    public void setMatno(String matno) {
        this.matno = matno;
    }

    public String getYear() {
        return year;
    }

    public Repeat year(String year) {
        this.year = year;
        return this;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Student getStudent() {
        return student;
    }

    public Repeat student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Repeat repeat = (Repeat) o;
        if (repeat.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), repeat.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Repeat{" +
            "id=" + getId() +
            ", matno='" + getMatno() + "'" +
            ", year='" + getYear() + "'" +
            "}";
    }
}
