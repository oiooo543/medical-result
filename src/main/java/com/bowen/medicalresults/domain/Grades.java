package com.bowen.medicalresults.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Grades.
 */
@Entity
@Table(name = "grades")
public class Grades implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "matno")
    private String matno;

    @Column(name = "ca")
    private Integer ca;

    @Column(name = "exam")
    private Integer exam;

    @Column(name = "total")
    private Integer total;

    @Column(name = "grade")
    private String grade;

    @Column(name = "semester")
    private Integer semester;

    @Column(name = "jhi_year")
    private String year;

    @Column(name = "attempt")
    private Integer attempt;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    @JsonIgnoreProperties("results")
    private Course course;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatno() {
        return matno;
    }

    public Grades matno(String matno) {
        this.matno = matno;
        return this;
    }

    public void setMatno(String matno) {
        this.matno = matno;
    }

    public Integer getCa() {
        return ca;
    }

    public Grades ca(Integer ca) {
        this.ca = ca;
        return this;
    }

    public void setCa(Integer ca) {
        this.ca = ca;
    }

    public Integer getExam() {
        return exam;
    }

    public Grades exam(Integer exam) {
        this.exam = exam;
        return this;
    }

    public void setExam(Integer exam) {
        this.exam = exam;
    }

    public Integer getTotal() {
        return total;
    }

    public Grades total(Integer total) {
        this.total = total;
        return this;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getGrade() {
        return grade;
    }

    public Grades grade(String grade) {
        this.grade = grade;
        return this;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getSemester() {
        return semester;
    }

    public Grades semester(Integer semester) {
        this.semester = semester;
        return this;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public String getYear() {
        return year;
    }

    public Grades year(String year) {
        this.year = year;
        return this;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getAttempt() {
        return attempt;
    }

    public Grades attempt(Integer attempt) {
        this.attempt = attempt;
        return this;
    }

    public void setAttempt(Integer attempt) {
        this.attempt = attempt;
    }

    public Boolean isStatus() {
        return status;
    }

    public Grades status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Course getCourse() {
        return course;
    }

    public Grades course(Course course) {
        this.course = course;
        return this;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Grades grades = (Grades) o;
        if (grades.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), grades.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Grades{" +
            "id=" + getId() +
            ", matno='" + getMatno() + "'" +
            ", ca=" + getCa() +
            ", exam=" + getExam() +
            ", total=" + getTotal() +
            ", grade='" + getGrade() + "'" +
            ", semester=" + getSemester() +
            ", year='" + getYear() + "'" +
            ", attempt=" + getAttempt() +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
