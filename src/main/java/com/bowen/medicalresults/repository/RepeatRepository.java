package com.bowen.medicalresults.repository;

import com.bowen.medicalresults.domain.Repeat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Repeat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RepeatRepository extends JpaRepository<Repeat, Long> {

}
