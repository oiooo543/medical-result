package com.bowen.medicalresults.service;

import com.bowen.medicalresults.service.dto.RepeatDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Repeat.
 */
public interface RepeatService {

    /**
     * Save a repeat.
     *
     * @param repeatDTO the entity to save
     * @return the persisted entity
     */
    RepeatDTO save(RepeatDTO repeatDTO);

    /**
     * Get all the repeats.
     *
     * @return the list of entities
     */
    List<RepeatDTO> findAll();


    /**
     * Get the "id" repeat.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RepeatDTO> findOne(Long id);

    /**
     * Delete the "id" repeat.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
