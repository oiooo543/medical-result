package com.bowen.medicalresults.service.mapper;

import com.bowen.medicalresults.domain.*;
import com.bowen.medicalresults.service.dto.GradesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Grades and its DTO GradesDTO.
 */
@Mapper(componentModel = "spring", uses = {CourseMapper.class})
public interface GradesMapper extends EntityMapper<GradesDTO, Grades> {

    @Mapping(source = "course.id", target = "courseId")
    GradesDTO toDto(Grades grades);

    @Mapping(source = "courseId", target = "course")
    Grades toEntity(GradesDTO gradesDTO);

    default Grades fromId(Long id) {
        if (id == null) {
            return null;
        }
        Grades grades = new Grades();
        grades.setId(id);
        return grades;
    }
}
