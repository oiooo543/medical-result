package com.bowen.medicalresults.service.mapper;

import com.bowen.medicalresults.domain.*;
import com.bowen.medicalresults.service.dto.RepeatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Repeat and its DTO RepeatDTO.
 */
@Mapper(componentModel = "spring", uses = {StudentMapper.class})
public interface RepeatMapper extends EntityMapper<RepeatDTO, Repeat> {

    @Mapping(source = "student.id", target = "studentId")
    RepeatDTO toDto(Repeat repeat);

    @Mapping(source = "studentId", target = "student")
    Repeat toEntity(RepeatDTO repeatDTO);

    default Repeat fromId(Long id) {
        if (id == null) {
            return null;
        }
        Repeat repeat = new Repeat();
        repeat.setId(id);
        return repeat;
    }
}
