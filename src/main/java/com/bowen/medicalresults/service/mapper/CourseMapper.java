package com.bowen.medicalresults.service.mapper;

import com.bowen.medicalresults.domain.*;
import com.bowen.medicalresults.service.dto.CourseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Course and its DTO CourseDTO.
 */
@Mapper(componentModel = "spring", uses = {DepartmentMapper.class})
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {

    @Mapping(source = "department.id", target = "departmentId")
    CourseDTO toDto(Course course);

    @Mapping(target = "results", ignore = true)
    @Mapping(source = "departmentId", target = "department")
    Course toEntity(CourseDTO courseDTO);

    default Course fromId(Long id) {
        if (id == null) {
            return null;
        }
        Course course = new Course();
        course.setId(id);
        return course;
    }
}
