package com.bowen.medicalresults.service.mapper;

import com.bowen.medicalresults.domain.*;
import com.bowen.medicalresults.service.dto.DepartmentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Department and its DTO DepartmentDTO.
 */
@Mapper(componentModel = "spring", uses = {FacultyMapper.class})
public interface DepartmentMapper extends EntityMapper<DepartmentDTO, Department> {

    @Mapping(source = "faculty.id", target = "facultyId")
    DepartmentDTO toDto(Department department);

    @Mapping(target = "students", ignore = true)
    @Mapping(target = "courses", ignore = true)
    @Mapping(source = "facultyId", target = "faculty")
    Department toEntity(DepartmentDTO departmentDTO);

    default Department fromId(Long id) {
        if (id == null) {
            return null;
        }
        Department department = new Department();
        department.setId(id);
        return department;
    }
}
