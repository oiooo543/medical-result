package com.bowen.medicalresults.service.mapper;

import com.bowen.medicalresults.domain.*;
import com.bowen.medicalresults.service.dto.StudentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Student and its DTO StudentDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, DepartmentMapper.class})
public interface StudentMapper extends EntityMapper<StudentDTO, Student> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "department.id", target = "departmentId")
    StudentDTO toDto(Student student);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "departmentId", target = "department")
    Student toEntity(StudentDTO studentDTO);

    default Student fromId(Long id) {
        if (id == null) {
            return null;
        }
        Student student = new Student();
        student.setId(id);
        return student;
    }
}
