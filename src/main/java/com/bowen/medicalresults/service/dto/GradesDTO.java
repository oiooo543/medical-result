package com.bowen.medicalresults.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Grades entity.
 */
public class GradesDTO implements Serializable {

    private Long id;

    private String matno;

    private Integer ca;

    private Integer exam;

    private Integer total;

    private String grade;

    private Integer semester;

    private String year;

    private Integer attempt;

    private Boolean status;


    private Long courseId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatno() {
        return matno;
    }

    public void setMatno(String matno) {
        this.matno = matno;
    }

    public Integer getCa() {
        return ca;
    }

    public void setCa(Integer ca) {
        this.ca = ca;
    }

    public Integer getExam() {
        return exam;
    }

    public void setExam(Integer exam) {
        this.exam = exam;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getAttempt() {
        return attempt;
    }

    public void setAttempt(Integer attempt) {
        this.attempt = attempt;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GradesDTO gradesDTO = (GradesDTO) o;
        if (gradesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gradesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GradesDTO{" +
            "id=" + getId() +
            ", matno='" + getMatno() + "'" +
            ", ca=" + getCa() +
            ", exam=" + getExam() +
            ", total=" + getTotal() +
            ", grade='" + getGrade() + "'" +
            ", semester=" + getSemester() +
            ", year='" + getYear() + "'" +
            ", attempt=" + getAttempt() +
            ", status='" + isStatus() + "'" +
            ", course=" + getCourseId() +
            "}";
    }
}
