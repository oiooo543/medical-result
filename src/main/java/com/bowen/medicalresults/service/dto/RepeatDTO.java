package com.bowen.medicalresults.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Repeat entity.
 */
public class RepeatDTO implements Serializable {

    private Long id;

    private String matno;

    private String year;


    private Long studentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatno() {
        return matno;
    }

    public void setMatno(String matno) {
        this.matno = matno;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RepeatDTO repeatDTO = (RepeatDTO) o;
        if (repeatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), repeatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RepeatDTO{" +
            "id=" + getId() +
            ", matno='" + getMatno() + "'" +
            ", year='" + getYear() + "'" +
            ", student=" + getStudentId() +
            "}";
    }
}
