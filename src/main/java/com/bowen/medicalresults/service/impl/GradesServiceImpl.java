package com.bowen.medicalresults.service.impl;

import com.bowen.medicalresults.service.GradesService;
import com.bowen.medicalresults.domain.Grades;
import com.bowen.medicalresults.repository.GradesRepository;
import com.bowen.medicalresults.service.dto.GradesDTO;
import com.bowen.medicalresults.service.mapper.GradesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Grades.
 */
@Service
@Transactional
public class GradesServiceImpl implements GradesService {

    private final Logger log = LoggerFactory.getLogger(GradesServiceImpl.class);

    private final GradesRepository gradesRepository;

    private final GradesMapper gradesMapper;

    public GradesServiceImpl(GradesRepository gradesRepository, GradesMapper gradesMapper) {
        this.gradesRepository = gradesRepository;
        this.gradesMapper = gradesMapper;
    }

    /**
     * Save a grades.
     *
     * @param gradesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public GradesDTO save(GradesDTO gradesDTO) {
        log.debug("Request to save Grades : {}", gradesDTO);
        Grades grades = gradesMapper.toEntity(gradesDTO);
        grades = gradesRepository.save(grades);
        return gradesMapper.toDto(grades);
    }

    /**
     * Get all the grades.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<GradesDTO> findAll() {
        log.debug("Request to get all Grades");
        return gradesRepository.findAll().stream()
            .map(gradesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one grades by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<GradesDTO> findOne(Long id) {
        log.debug("Request to get Grades : {}", id);
        return gradesRepository.findById(id)
            .map(gradesMapper::toDto);
    }

    /**
     * Delete the grades by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Grades : {}", id);
        gradesRepository.deleteById(id);
    }
}
