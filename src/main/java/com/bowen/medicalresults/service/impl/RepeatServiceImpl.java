package com.bowen.medicalresults.service.impl;

import com.bowen.medicalresults.service.RepeatService;
import com.bowen.medicalresults.domain.Repeat;
import com.bowen.medicalresults.repository.RepeatRepository;
import com.bowen.medicalresults.service.dto.RepeatDTO;
import com.bowen.medicalresults.service.mapper.RepeatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Repeat.
 */
@Service
@Transactional
public class RepeatServiceImpl implements RepeatService {

    private final Logger log = LoggerFactory.getLogger(RepeatServiceImpl.class);

    private final RepeatRepository repeatRepository;

    private final RepeatMapper repeatMapper;

    public RepeatServiceImpl(RepeatRepository repeatRepository, RepeatMapper repeatMapper) {
        this.repeatRepository = repeatRepository;
        this.repeatMapper = repeatMapper;
    }

    /**
     * Save a repeat.
     *
     * @param repeatDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RepeatDTO save(RepeatDTO repeatDTO) {
        log.debug("Request to save Repeat : {}", repeatDTO);
        Repeat repeat = repeatMapper.toEntity(repeatDTO);
        repeat = repeatRepository.save(repeat);
        return repeatMapper.toDto(repeat);
    }

    /**
     * Get all the repeats.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RepeatDTO> findAll() {
        log.debug("Request to get all Repeats");
        return repeatRepository.findAll().stream()
            .map(repeatMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one repeat by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RepeatDTO> findOne(Long id) {
        log.debug("Request to get Repeat : {}", id);
        return repeatRepository.findById(id)
            .map(repeatMapper::toDto);
    }

    /**
     * Delete the repeat by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Repeat : {}", id);
        repeatRepository.deleteById(id);
    }
}
