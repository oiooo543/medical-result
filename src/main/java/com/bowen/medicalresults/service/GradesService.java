package com.bowen.medicalresults.service;

import com.bowen.medicalresults.service.dto.GradesDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Grades.
 */
public interface GradesService {

    /**
     * Save a grades.
     *
     * @param gradesDTO the entity to save
     * @return the persisted entity
     */
    GradesDTO save(GradesDTO gradesDTO);

    /**
     * Get all the grades.
     *
     * @return the list of entities
     */
    List<GradesDTO> findAll();


    /**
     * Get the "id" grades.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<GradesDTO> findOne(Long id);

    /**
     * Delete the "id" grades.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
